package graph;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

import org.graphstream.*;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import javax.swing.*;

import static java.lang.Integer.max;

public class GraphMatrix implements Serializable {

	// Class atributes

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int[][] matrix;
	private int size;

	private final int RESIZE_FACTOR = 1;

	// Constructor

	public GraphMatrix(int size) {
		this.size = size;
		this.matrix = new int[size][size];

		for (int i = 0; i < this.matrix.length; i++) {

			for (int j = 0; j < this.matrix[i].length; j++) {
				this.matrix[i][j] = Integer.MAX_VALUE;

			}
			size = this.matrix.length;

		}
	}
	// Class methods

	// Methods addConnection add two vertices and connection between them
	public void addConnection(Connection connection) {
		if (connection.getA() >= size || connection.getB() >= size)
			resize(max(connection.getA(), connection.getB()));
		matrix[connection.getA()][connection.getB()] = connection.getWeight();
		matrix[connection.getB()][connection.getA()] = connection.getWeight();
		matrix[connection.getA()][connection.getA()] = 0;
		matrix[connection.getB()][connection.getB()] = 0;
	}

	public void addConnection(int a, int b, int weight) {
		if (a >= size || b >= size)
			resize(max(a, b));
		matrix[a][b] = weight;
		matrix[b][a] = weight;
		matrix[a][a] = 0;
		matrix[b][b] = 0;
	}

	public void addConnection(int a, int b) {
		if (a >= size || b >= size)
			resize(max(a, b));
		matrix[a][b] = 1;
		matrix[b][a] = 1;
		matrix[a][a] = 0;
		matrix[b][b] = 0;
	}

	// addVertex adds ONLY one vertex
	public void addVertex(int a) {
		if (a >= size)
			resize(a);
		matrix[a][a] = 0;
	}

	// connectVertices connects two vertices ONLY if they both exist
	public void connectVertices(int a, int b, int weight) {
		if (matrix[a][a] != Integer.MAX_VALUE && matrix[b][b] !=Integer.MAX_VALUE) {
			matrix[a][b] = weight;
			matrix[b][a] = weight;
		} else {
			throw new NullPointerException();
		}
	}

	// deletes connection after checking if it exist
	public void deleteConnection(int a, int b) {
		if (matrix[a][b] != Integer.MAX_VALUE && matrix[b][a] != Integer.MAX_VALUE) {
			matrix[a][b] = Integer.MAX_VALUE;
			matrix[b][a] = Integer.MAX_VALUE;
		} else {
			throw new NullPointerException();
		}
	}

	// deletes connection even if it doesn't exist
	public void eraseConnection(int a, int b) {
		matrix[a][b] = Integer.MAX_VALUE;
		matrix[b][a] = Integer.MAX_VALUE;
	}

	// deletes vertex ONLY if it exists
	public void deleteVertex(int a) {
		if (matrix[a][a] != Integer.MAX_VALUE) {
			matrix[a][a] = Integer.MAX_VALUE;
			
		} else {
			throw new NullPointerException();
		}
	}

	// Resize matrix
	public void resize(int a) {
		int[][] new_matrix = Arrays.copyOf(matrix, a + RESIZE_FACTOR);
		for (int i = 0; i < matrix.length; i++) {
			new_matrix[i] = Arrays.copyOf(matrix[i], a + RESIZE_FACTOR);
		}
		for (int i = matrix.length; i < new_matrix.length; i++) {
			new_matrix[i] = new int[new_matrix[0].length];
		}
		matrix = new_matrix;
		size = new_matrix.length;
		System.gc();
	}

	// Displaying all connections in console
	public void displayConnections() {
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i][i] != Integer.MAX_VALUE) { // if element i exists, check all connections to this element, but only with j>i
										// (to avoid duplicates)
				for (int j = i + 1; j < matrix[0].length; j++) {
					if (matrix[i][j] != Integer.MAX_VALUE)
						System.out.println("Connection between " + i + " and " + j + " has weight " + matrix[i][j]);
				}
			}
		}
	}

	// Serialization
	public void saveMatrix(String filename) {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))) {
			out.writeObject(matrix);
		} catch (IOException e) {
			e.printStackTrace();
		}
		;
	}

	// Deserialization
	public void loadMatrix(String filename) {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
			matrix = (int[][]) in.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}

	public int[][] getMatrix() {
		return matrix;
	}

	public GraphMatrix PrimAlgo(){
		GraphMatrix graph = new GraphMatrix(matrix.length);
		int[] visited = new int[matrix.length];
		ArrayList<Connection> connections = new ArrayList<>();

		for(int i = 0; i < matrix.length; i++){
			if(matrix[i][i] == Integer.MAX_VALUE){
				visited[i] = Integer.MAX_VALUE; //vertex doesn't exist
			}
		}

		int current_vertex = firstNotVisited(visited);

		while(!allVisited(visited)){
			visited[current_vertex] = 1;
			for(int i = 0; i < matrix[current_vertex].length; i++){
				if(matrix[current_vertex][i] != Integer.MAX_VALUE) {
					connections.add(new Connection(current_vertex, i, matrix[current_vertex][i]));
				}
			}
			connections.sort(new Comparator<Connection>() {
				@Override
				public int compare(Connection o1, Connection o2) {
					return o1.getWeight()-o2.getWeight();
				}
			});
			Connection current_connnection = connections.get(0);
			while(connections.size() > 1 && (current_connnection.getB() == current_vertex || visited[current_connnection.getB()] == 1)){
				connections.remove(0);
				current_connnection = connections.get(0);
			}
			if(current_connnection.getB() != current_vertex && visited[current_connnection.getB()] != 1) {
				graph.addConnection(current_connnection);
				current_vertex = current_connnection.getB();
			}else{
				current_vertex = firstNotVisited(visited);
			}

		}


		return graph;
	}
	public void displayPrim(int interval) throws InterruptedException {
		SingleGraph graph = new SingleGraph("GraphPrime");
		graph.addAttribute("ui.stylesheet", styleSheet);
		Viewer viewer = graph.display();
		for(int i = 0; i < matrix.length; i++){
			if(matrix[i][i] != Integer.MAX_VALUE){
				graph.addNode(String.valueOf(i));
				Node node = graph.getNode(String.valueOf(i));
				node.setAttribute("ui.label", i);
			}
		}
		int[] visited = new int[matrix.length];
		ArrayList<Connection> connections = new ArrayList<>();

		for(int i = 0; i < matrix.length; i++){
			if(matrix[i][i] == Integer.MAX_VALUE){
				visited[i] = Integer.MAX_VALUE; //vertex doesn't exist
			}
		}

		int current_vertex = firstNotVisited(visited);

		while(!allVisited(visited)){
			Thread.sleep(interval);
			visited[current_vertex] = 1;
			for(int i = 0; i < matrix[current_vertex].length; i++){
				if(matrix[current_vertex][i] != Integer.MAX_VALUE) {
					connections.add(new Connection(current_vertex, i, matrix[current_vertex][i]));
				}
			}
			connections.sort(new Comparator<Connection>() {
				@Override
				public int compare(Connection o1, Connection o2) {
					return o1.getWeight()-o2.getWeight();
				}
			});
			Connection current_connnection = connections.get(0);
			while(connections.size() > 1 && (current_connnection.getB() == current_vertex || visited[current_connnection.getB()] == 1)){
				connections.remove(0);
				current_connnection = connections.get(0);
			}
			if(current_connnection.getB() != current_vertex && visited[current_connnection.getB()] != 1) {
				graph.addEdge(String.valueOf(current_connnection.getA())+" "+String.valueOf(current_connnection.getB()), String.valueOf(current_connnection.getA()), String.valueOf(current_connnection.getB()));
				Edge edge = graph.getEdge(String.valueOf(current_connnection.getA())+" "+String.valueOf(current_connnection.getB()));
				edge.setAttribute("ui.label", current_connnection.getWeight());
				current_vertex = current_connnection.getB();
			}else{
				current_vertex = firstNotVisited(visited);
			}
		}
	}

	public GraphMatrix PrimAlgorithm() {
		//nowy graf

		GraphMatrix graph = new GraphMatrix(matrix.length);
		//macierz na zaznaczanie ktory wierzcholek juz odwiedzony
		int [] visited = new int[matrix.length];
		int min = Integer.MAX_VALUE;
		//na krawedz do mst
		int u = 0;
		int v = 0;
	
		/*// wyzerowanie nowej macierzy na drzewo spinajace
		for (int d = 0; d < matrix2.length; d++) {
			for (int e = 0; e < matrix2[d].length; e++) {
				matrix2[d][e] = 0;
			}
		}*/
		int vertex = 0;

		for (int i = 0; i < visited.length; i++) {

			visited[i] = 1;
			for (int j = 0; j < matrix[i].length; j++) {

				if (matrix[i][j] == 0 ){
					matrix[i][j] = Integer.MAX_VALUE;

				}
				if(matrix[i][j] == 1) {
					matrix[i][j] = Integer.MAX_VALUE;
					if(i==j) {
					vertex++;}
				}
				
				if (matrix[i][j]!=Integer.MAX_VALUE) {
					visited[i] = 0;

				}

			}
			visited[0] = 1;
			//algorytm
			
		
			//tyle ile wierzcholkow -1 razy
			for (int count = 0; count < vertex-1; count++) {

				min = Integer.MAX_VALUE;

				for (int a = 0; a < matrix.length; a++) {
					if (visited[a] == 1) {

						for (int b = 0; b < matrix[a].length; b++) {
							if (visited[b] ==0) {
								if (min > matrix[a][b]) {
									min = matrix[a][b];

									//polaczenie do dodania do nowego grafu
									u = a;
									v = b;
							
								}
							}

						}

					}

				}

				visited[u] = 1;
				visited[v] = 1;
				if(min!=Integer.MAX_VALUE)
				{
				graph.addConnection(u, v, min);}

				matrix[u][v] = matrix[u][v] = Integer.MAX_VALUE;
			//matrix2[u][v] = min;
			//matrix2[v][u] = min;

			}

		}
		System.out.println("newgraph");
		return graph;
	}

	private boolean allVisited(int[] tab){
		for(int i = 0; i<tab.length; i++){
			if(tab[i] == 0) return false;
		}
		return true;
	}
	private int firstNotVisited(int[] tab){
		for(int i = 0; i<tab.length; i++){
			if(tab[i] == 0) return i;
		}
		return -1;
	}

	public void displayGraph() throws InterruptedException {
		Graph graph = new SingleGraph("Graph");
		graph.addAttribute("ui.stylesheet", styleSheet);
		for(int i = 0; i < matrix.length; i++){
			if(matrix[i][i] != Integer.MAX_VALUE){
				graph.addNode(String.valueOf(i));
				Node node = graph.getNode(String.valueOf(i));
				node.setAttribute("ui.label", i);
			}
		}
		for(int i = 0; i < matrix.length; i++){
			if(matrix[i][i] != Integer.MAX_VALUE){
				for(int j = i+1; j < matrix[0].length; j++){
					if(matrix[i][j] != Integer.MAX_VALUE){
						graph.addEdge(String.valueOf(i)+" "+String.valueOf(j), String.valueOf(i), String.valueOf(j));
						Edge edge = graph.getEdge(String.valueOf(i)+" "+String.valueOf(j));
						edge.setAttribute("ui.label", matrix[i][j]);
					}
				}
			}
		}
		graph.display();
	}
	protected String styleSheet =
			"node {" +
					"	fill-color: red;" +
					"}" +
					"node.marked {" +
					"	fill-color: green;" +
					"}"+
					"edge {" +
			"	fill-color: black;" +
			"}" +
			"node.marked {" +
			"	fill-color: red;" +
			"}";

}

