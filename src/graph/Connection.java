package graph;

public class Connection {

	//Class atributes

    private int a;
    private int b;
    private int weight;
    

    //Constructors

    public Connection(int a, int b, int weight){
        this.a = a;
        this.b = b;
        this.weight = weight;
    }
    public Connection(int a, int b){
        this.a = a;
        this.b = b;
        this.weight = 1;
    }

    //Getters

    public int getA(){
        return a;
    }
    public int getB(){
        return b;
    }
    public int getWeight(){
        return weight;
    }

    //Setters

    public void setWeight(int new_weight){
        this.weight = new_weight;
    }

    public String toString(){
        return "Connection between "+getA()+" and "+getB()+" has weight "+getWeight();
    }
}
